package main

import (
	"fmt"
	"log"

	"gitlab.com/antoine101/go-greetings"
)

func main() {
	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	var messages map[string]string
	var err error
	names := []string{"Tom", "Harold", "Simon", "Julien"}
	messages, err = greetings.Hellos(names)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(messages)
}
